// Include libs
#include <ESP8266WiFi.h>
#include "DHT.h"

//Define constant
#define DHTTYPE DHT11 
#define dht_apin 14

// Init library
WiFiClient client;

//Init DHT Sensor
DHT dht(dht_apin, DHTTYPE);

//Wi-fi config ssid is your AP name , password is your AP Secret code
const char* ssid = "E520";  
const char* password = "nkfuste520wifi";

// Get data host & port
const char* host = "192.168.0.104"; // Your domain    

//POST String
String postStr = "";

// Sensor values
float h,t,hic;

void setup() {  

  //Init DHT sensor
  Serial.begin(9600);
  dht.begin();
  
  // Connect to wifi
  connectWifi();

  // Print wifi status
  printWifiStatus();
   
}

void loop() {  

  //Read DHT sensor data
  readSensor();

  // Give the post data the time to get the delay to get the data.
  delay(500);
  
  postData();
  
}

//Function for ESP8266 connect to AP
void connectWifi() {

  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  //If Wi-Fi status doesn't connect ,were try to connect again
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  } 
}

//Function for ESP8266 Wi-Fi status
void printWifiStatus() {
  // Print the SSID of the network you're attached to
  Serial.print("Connected to ");
  Serial.println(ssid);

  // Print your Wi-Fi shield's IP address
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  
}

//Function for Getting the DHT11 data
void readSensor() {
    t = dht.readTemperature();
    hic = dht.computeHeatIndex(t, h, false);
    static char temperatureTemp[7];
    //float to char
    dtostrf(hic, 6, 2, temperatureTemp);
   
    h = dht.readHumidity();
    static char humidityTemp[7];
    //float to char
    dtostrf(h, 6, 2, humidityTemp);
  
  Serial.print("Current humidity = ");
  
  Serial.print(h);
  
  Serial.print("%  ");
  
  Serial.print("temperature = ");
  
  Serial.print(hic); 
  Serial.println("C  ");
  
}

void postData() {
  
  // Bind sensor value to post url;
   postStr =postStr+"temp=" + hic + "&humid=" + h;

  // If there's a successful connection, send the HTTP POST request
  if (client.connect(host, 80)) {

    client.println("POST /weather-app/includes/app/addItem.php HTTP/1.1");
    client.println("Host: 192.168.0.104");
    client.println("Connection: close");
    client.println("Content-Type: application/x-www-form-urlencoded;");
    client.print("Content-Length: ");
    client.println(postStr.length());
    client.println();
    client.println(postStr);

    // Print post data
    while(client.available()) {
      char c = client.read();
      Serial.print(c);    
    }
    
    // give post enough time 
    delay(5000); 

    // Print post url + data
    Serial.println(postStr);

    // Empty string again for the next data or it will add up :)
    postStr = "";
    
    
  } else {
    
    // If connection failed
    Serial.println("UH OH! Connection failed");
    
  }
 
}