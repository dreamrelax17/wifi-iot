var _switch = document.querySelector('.switch');

var _body = Ld('body');
var _menuTrigger = Ld('#menu-trigger');

_switch.addEventListener('change', function(e) {

    var _isChecked = e.target.checked;

    if(_isChecked) {

        _timer = setTimeout(function(){

            window.location = '?handle=manual';
            clearTimeout(_timer);

        }, 600);


    } else {

        _timer = setTimeout(function(){

            window.location = '?handle=auto';
            clearTimeout(_timer);

        }, 600);

    }

});

_menuTrigger.addListener('click', function(e){
    e.preventDefault();
    _body.toggleClass('menu-open');
});
