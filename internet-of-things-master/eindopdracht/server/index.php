<?php
	include("includes/_base/head.php");

	$pauze = strtotime("+30 seconds");
	$date = date('Y-m-d H:i:s');
	$pauzeDate = date('Y-m-d H:i:s',  $pauze);
?>

<?php include("includes/_base/header.php"); ?>

	<main>

		<?php include("includes/modules/line-chart.php"); ?>

	</main> <!-- end main -->


<script src="//cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
<script src="js/xhr.js"></script>
<script src="js/ld.js"></script>
<script src="js/peach-min.js"></script>
<script src="js/script.js"></script>	

<?php include("includes/_base/footer.php"); ?>
