<header class="main-header">
	<a href="#" id="menu-trigger">
		<i class="menu-icon"></i>
	</a>

	<a href="/" class="logo">
		<img src="img/dylan.png" alt="dylan vens">
	</a>

	<nav>
		<ul class="main-nav">
			<li><a href="index.php">Dashboard</a></li>
			<li> <a href="settings.php">Settings</a> </li>
		</ul>

		<ul class="social-nav">
			<li class="copy"> &copy; 2016 by Dylan Vens </li>
		</ul>
	</nav> <!-- end nav -->

</header> <!-- end .main-header -->
