<?php 
	
	require('connection.php');

	$date = date('Y-m-d H:i:s');

	$jsonHandle = file_get_contents('../../data/handle.json');
	$dataHandle = json_decode($jsonHandle, true);

	$jsonSettings = file_get_contents('../../data/settings.json');
	$dataSettings = json_decode($jsonSettings, true);

	function setData($conn, $date) {

		$value = mysqli_real_escape_string($conn ,$_POST['value']);

		$sql = "INSERT INTO sensor (ID, value, date)
		VALUES (NULL, '$value', '$date')";

		mysqli_query($conn, $sql);
    		
	}

	if(isset($_POST['value'])) {

		setData($conn, $date);	

	}

	if($dataHandle['handle'] == 'auto') {

		$timeNow = strtotime(date('Y-m-d H:i:s'));
		$startTime = strtotime($dataSettings['pause']['startTime']);
		$endTime = strtotime($dataSettings['pause']['endTime']);

		$startWorkTime = strtotime($dataSettings['work']['startTime']);
		$endWorkTime = strtotime($dataSettings['work']['endTime']);		

		// Set status data
		$sensorValue = $_POST['value'];
		$warning = false;
		$pause = false;
		$buzzer = 'off';
		$greenLed = 'off';
		$yellowLed = 'off';
		$redLed = 'off';
		$status = '';

		$jsonString = file_get_contents('../../data/status.json');
		$data = json_decode($jsonString, true);

		if($timeNow > $endTime && $timeNow > $startTime && $timeNow < $endWorkTime) {
			$buzzer = 'off';
			$yellowLed = 'off';
			$greenLed = 'on';
			$redLed = 'off';
			$status = 'sitting';
		} 

		if($timeNow >= $startTime && $timeNow < $endTime) {
			$buzzer = 'off';
			$yellowLed = 'on';
			$greenLed = 'off';
			$redLed = 'off';
			$status = 'pause';
		} 

		if($timeNow >= $startTime && $timeNow < $endTime && $sensorValue == 1) {
			$buzzer = 'on';
			$yellowLed = 'off';
			$greenLed = 'off';
			$redLed = 'on';
			$status = 'warning';
		} 

		if($timeNow >= $endWorkTime) {
			$buzzer = 'off';
			$yellowLed = 'off';
			$greenLed = 'off';
			$redLed = 'off';
			$status = 'done';
		}

		$data['dateTime'] = $date;
		$data['yellowLed'] = $yellowLed;
		$data['greenLed'] = $greenLed;
		$data['redLed'] = $redLed;
		$data['buzzer'] = $buzzer;
		$data['status'] = $status;
		$data['value'] = $sensorValue;

		$newJsonString = json_encode($data);
		file_put_contents('../../data/status.json', $newJsonString);

	}
	
?>