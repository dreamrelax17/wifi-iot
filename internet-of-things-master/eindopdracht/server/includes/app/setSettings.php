<?php 
	
	$pauseTime = isset($_POST['pause']) ? $_POST['pause'] : "0 seconds";
	$workingTime = isset($_POST['work']) ? $_POST['work'] : "0 seconds";

	$jsonSettings = file_get_contents('../../data/settings.json');
	$dataSettings = json_decode($jsonSettings, true);

	$dataSettings['pause']['time'] = $pauseTime;
	$dataSettings['work']['time'] = $workingTime;
	
	$dataSettings['pause']['startTime'] = date('Y-m-d H:i:s');
	$dataSettings['work']['startTime'] = date('Y-m-d H:i:s');

	$dataSettings['pause']['endTime'] = date('Y-m-d H:i:s', strtotime("+" . $pauseTime));
	$dataSettings['work']['endTime'] = date('Y-m-d H:i:s', strtotime("+" . $workingTime));

	$newJsonString = json_encode($dataSettings);
	file_put_contents('../../data/settings.json', $newJsonString);

	header('Location: ../../settings.php');

?>