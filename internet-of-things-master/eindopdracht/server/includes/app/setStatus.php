<?php 
	
	
	require('connection.php');

	// // Set status data
	$buzzer = isset($_POST['buzzer']) ? $_POST['buzzer'] : 'off';
	$greenLed = isset($_POST['greenLed']) ? $_POST['greenLed'] : 'off';
	$yellowLed = isset($_POST['yellowLed']) ? $_POST['yellowLed'] : 'off';
	$redLed = isset($_POST['redLed']) ? $_POST['redLed'] : 'off';
	
	$jsonString = file_get_contents('../../data/status.json');
	$data = json_decode($jsonString, true);

	function getData($conn) {

		$sql = "SELECT * FROM sensor WHERE value=0";
	    $result = mysqli_query($conn, $sql) or die("Error in Selecting " . mysqli_error($conn));

	   
	    // Create array
	    $sensorArray = Array();

	    while( $row = mysqli_fetch_assoc($result) ) {

	    	$sensorArray[] = $row;

	    }

		$amount = sizeof($sensorArray);
		return round($amount / 60);

	}

	function getHours($conn) {

		$sql = "SELECT * FROM sensor WHERE value=1";
	    $result = mysqli_query($conn, $sql) or die("Error in Selecting " . mysqli_error($conn));

	   
	    // Create array
	    $sensorArray = Array();

	    while( $row = mysqli_fetch_assoc($result) ) {

	    	$sensorArray[] = $row;

	    }

		$amount = sizeof($sensorArray);
		return round($amount / 60);

	}

	$data['breaks'] = getData($conn);
	$data['hours'] = getHours($conn);
	$data['yellowLed'] = $yellowLed;
	$data['greenLed'] = $greenLed;
	$data['redLed'] = $redLed;
	$data['buzzer'] = $buzzer;

	$newJsonString = json_encode($data);
	file_put_contents('../../data/status.json', $newJsonString);

	header('Location: ../../settings.php');

?>