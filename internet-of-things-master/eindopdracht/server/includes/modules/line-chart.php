<?php 
	$jsonSettings = file_get_contents('./data/settings.json');
	$dataSettings = json_decode($jsonSettings, true);
?>

<section class="line-chart -tx-center">

	<h2 class="section-title">Sensor Data</h2>
	
	<div class="row"></div>

	<div class="loader"></div>
	<div id="chart"></div>

</section>

<template id="status">
	
	<div class="col">
		{% if(status === 'sitting'){ %}
			<p class="status--sit">You can work and sit till: <br><?php echo $dataSettings['work']['endTime']; ?></p>
		{% } %}
		
		{% if(status === 'warning'){ %}
			<p class="status--warning">Get of your chair and take a break till: <br><?php echo $dataSettings['pause']['endTime']; ?></p>
		{% } %}
		
		{% if(status === 'pause'){ %}
			<p class="status--pause">Take a break! Till: <br><?php echo $dataSettings['pause']['endTime']; ?></p>
		{% } %}

		{% if(status === 'done'){ %}
			<p class="status--done">Your done for today!</p>
		{% } %}
	</div>

	<div class="col">
		<p class="normal">Amount of working/study hours overall: <span>{{ hours }} hours</span></p>
	</div>

	<div class="col">
		<p class="normal">Amount of breaks overall: <span>{{ breaks }} breaks</span></p>
	</div>

</template>