<?php

	$handle = isset($_GET['handle']) ? $_GET['handle'] : '';

	$jsonString = file_get_contents('./data/status.json');
	$data = json_decode($jsonString, true);

	$jsonSettings = file_get_contents('./data/settings.json');
	$dataSettings = json_decode($jsonSettings, true);

	if( ISSET($_GET['handle']) ) {

		if($handle == "manual") {
			$file = fopen("./data/handle.json", "w") or die("can't open file");
			fwrite($file, '{"handle": "manual"}');
			fclose($file);
		} else if ($handle == "auto") {
			$file = fopen("./data/handle.json", "w") or die("can't open file");
			fwrite($file, '{"handle": "auto"}');
			fclose($file);
		}

	}
?>

<section class="buttons -tx-center">
	<h2 class="section-title">Settings</h2>
	<h2>
		<?php
			if($handle=="manual") {
				echo("You can now take over the controls");
			}
			else if ($handle=="auto") {
				echo("You can not use the buttons to control the lamps and buzzer");
			}
			else {
				echo ("Set the controls to manual or auto");
			}
		?>
	</h2>

	<div class="switch switch--one">
  		<input id="switch-1" class="switch__toggle" <?php if($handle == "manual") { echo "checked"; } ?> type="checkbox">
  		<label for="switch-1"></label>
	</div>

	<?php if($handle == "manual") {?>

	<h2>States and buzzer</h2>

	<form action="./includes/app/setStatus.php" method="POST">

		<fieldset>
								
			<legend>Control the buzzer and states</legend>
			
			<label>
				<span>Pause on</span>
				<input name="yellowLed" type="radio" value="on" <?php if($data["yellowLed"] == "on") { echo "checked"; } ?> >

				<span>Pause off</span>
				<input name="yellowLed" type="radio" value="off" <?php if($data["yellowLed"] == "off") { echo "checked"; } ?>>
			</label>

			<label>
				<span>Warning on</span>
				<input name="redLed" type="radio" value="on" <?php if($data["redLed"] == "on") { echo "checked"; } ?>>

				<span>Warning off</span>
				<input name="redLed" type="radio" value="off" <?php if($data["redLed"] == "off") { echo "checked"; } ?>>
			</label>

			<label>
				<span>Green light on</span>
				<input name="greenLed" type="radio" value="on" <?php if($data["greenLed"] == "on") { echo "checked"; } ?>>

				<span>Green light off</span>
				<input name="greenLed" type="radio" value="off" <?php if($data["greenLed"] == "off") { echo "checked"; } ?>>
			</label>

			<label>
				<span>Buzzer on</span>
				<input name="buzzer" type="radio" value="on" <?php if($data["buzzer"] == "on") { echo "checked"; } ?>>

				<span>Buzzer off</span>
				<input name="buzzer" type="radio" value="off" <?php if($data["buzzer"] == "off") { echo "checked"; } ?>>
			</label>

			
			<button role="button" type="submit">Save</button>

		</fieldset>


	</form>

	<?php } ?>

	<h2>Pause and hour settings</h2>

		<form action="./includes/app/setSettings.php" method="POST">
		
		<fieldset>
								
			<legend>Set working/study hours</legend>
			
			<label>
				<span>Ending the day in:</span>
				<input name="work" type="text" placeholder="Set amount of working/study hours.." value="<?php echo $dataSettings['work']['time'] ?>">
				<input name="pause" type="hidden" placeholder="Set new pause time.." value="<?php echo $dataSettings['pause']['time'] ?>">
				<div></div>
			</label>
			
			<button role="button" type="submit">Save</button>

		</fieldset>

	</form>
	
	<form action="./includes/app/setSettings.php" method="POST">
		
		<fieldset>
								
			<legend>Set the hours/minutes or seconds for your pause:</legend>
			
			<label>
				<span>Pause time for about:</span>
				<input name="pause" type="text" placeholder="Set new pause time.." value="<?php echo $dataSettings['pause']['time'] ?>">
				<input name="work" type="hidden" placeholder="Set amount of working/study hours.." value="<?php echo $dataSettings['work']['time'] ?>">
				<div></div>
			</label>
			
			<button role="button" type="submit">Take a break</button>

		</fieldset>

	</form>

</section>
