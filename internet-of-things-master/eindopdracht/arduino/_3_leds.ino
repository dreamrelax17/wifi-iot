// Include libs
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>

// Init wifi 
WiFiClient client;

// Wifi config, ssid = wifi name & password = wifi password
char ssid[] = "Dylan"; //  Network Name
char password[] = "123dvens"; // Network Password

// Get data host & port
char* host = "www.dylanvens.com"; // Your domain 
const int httpPort = 80;

// post string & get json url
String postStr = "";
String path = "/iot-week-3/data/status.json";

// Store constants
const int greenLed = D6;
const int yellowLed = D7;
const int redLed = D8;
const int buttonPin = D4;
const int buzzerPin = D3;

// Set values
int buttonValue = 0;

// Set intervals for sending and posting data 
// Used: https://learn.adafruit.com/multi-tasking-the-arduino-part-1/using-millis-for-timing
const long sendInterval = 10000; // 10 seconds
const long getInterval = 500; // 0.5 seconds

// Set value timers
long sendPreviousMillis = 0;
long getPreviousMillis = 0;

void setup() {  
  
  Serial.begin(9600);

  // Connect to wifi
  connectWifi();

  // Print wifi status
  printWifiStatus();

  // Set lights
  setLights();
   
}

void loop() {
  
  unsigned long currentMillis = millis();

  if(currentMillis - sendPreviousMillis > sendInterval) {
    
    sendPreviousMillis = currentMillis;   
    postData();
   
  }

  if(currentMillis - getPreviousMillis > getInterval) {
    
    getPreviousMillis = currentMillis;   
    getData();
   
  }
  
}

void getData() {

  // Inspired by http://blog.nyl.io/esp8266-led-arduino/
  // Print if not connected
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
  }
  
  client.print(String("GET ") + path + " HTTP/1.1\r\n" + 
    "Host: " + host + "\r\n" + 
    "Connection: keep-alive\r\n\r\n");  

  delay(500);

  String section="header";
  
  while(client.available()){
  
    String line = client.readStringUntil('\r');
    
    // Serial.print(line);
    // we’ll parse the HTML body here
    if (section=="header") { // headers..
    
      Serial.print(".");
      
      if (line=="\n") { // skips the empty space at the beginning 
        section="json";
      }
      
    } else if (section=="json") {  // print the good stuff
      
      section="ignore";
      String result = line.substring(1);

      // Parse JSON
      int size = result.length() + 1;
      char json[size];
      result.toCharArray(json, size);
      StaticJsonBuffer<200> jsonBuffer;
      JsonObject& json_parsed = jsonBuffer.parseObject(json);
      if (!json_parsed.success())
      {
        Serial.println("parseObject() failed");
        return;
      }

      if (strcmp(json_parsed["yellowLed"], "on") == 0) {
        digitalWrite(yellowLed, HIGH); 
      } else {
        digitalWrite(yellowLed, LOW); 
      }

      if (strcmp(json_parsed["redLed"], "on") == 0) {
        digitalWrite(redLed, HIGH); 
      } else {
        digitalWrite(redLed, LOW); 
      }

      if (strcmp(json_parsed["greenLed"], "on") == 0) {
        digitalWrite(greenLed, HIGH); 
      } else {
        digitalWrite(greenLed, LOW); 
      }

      if (strcmp(json_parsed["buzzer"], "on") == 0) {
        buzz(50); 
      } else {
        analogWrite(buzzerPin,0);
      }
      
    }
  } 
  
}

void postData() {
  
  // Get sensorValue
  buttonValue = digitalRead(buttonPin);

  // Bind sensor value to post url
  postStr += "value=" + String(buttonValue);

  // If there's a successful connection, send the HTTP POST request
  if (client.connect(host, 80)) {

    client.println("POST /iot-week-3/includes/app/addItem.php HTTP/1.1");
    client.println("Host:" + String(host));
    client.println("Connection: close");
    client.println("Content-Type: application/x-www-form-urlencoded;");
    client.print("Content-Length: ");
    client.println(postStr.length());
    client.println();
    client.println(postStr);

    // Print post data
    while(client.available()) {
      char c = client.read();
      Serial.print(c);    
    }
    
    // give post enough time 
    delay(500); 

    // Print post url + data
    Serial.println(postStr);

    // Empty string
    postStr = "";
    
    
  } else {
    
    // If connection failed
    Serial.println("UH OH! Connection failed");
    
  }
 
}

void buzz(unsigned char time) {
    analogWrite(buzzerPin,200);
    delay(time);
    analogWrite(buzzerPin,0);
    delay(time);
}

void setLights() {
  pinMode(greenLed, OUTPUT);
  pinMode(yellowLed, OUTPUT);
  pinMode(redLed, OUTPUT);
  pinMode(buzzerPin, OUTPUT);
  pinMode(buttonPin, INPUT);
}

void connectWifi() {

  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
}

void printWifiStatus() {
  // Print the SSID of the network you're attached to
  Serial.print("Connected to ");
  Serial.println(ssid);

  // Print your WiFi shield's IP address
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  
}




