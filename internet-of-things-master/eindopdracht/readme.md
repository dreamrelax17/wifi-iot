# How to create a system that tells you when to take a break from studying or working?
I work/study alot behind my desk and most of the time I forget to take a break or just quit working/studying for a long period of time. So I made a system that tells me when to stop working/studying and when to take a break of studying/working. It has three phases: 
* Green tells me I can sit and work/study, 
* Orange tells me to take a break 
* Gray tells me to stop
* Red tells me to take a break when I'm ignoring the breaks. 
* Link to the product: [The product dashboard](http://www.dylanvens.com/iot-week-3/)
* Link to the arduino code: [https://github.com/dvens/internet-of-things/blob/master/eindopdracht/arduino/_3_leds.ino](https://github.com/dvens/internet-of-things/blob/master/eindopdracht/arduino/_3_leds.ino)

# Api
The dashboard has an Api that publishes real time data.

## [GET] Status
Get the current status of the dashboard: [www.dylanvens.com/iot-week-3/data/status.json](http://www.dylanvens.com/iot-week-3/data/status.json)

```json
    {
        dateTime: "2016-05-19 12:10:32",
        yellowLed: "off",
        greenLed: "off",
        redLed: "off",
        buzzer: "off",
        status: "done",
        value: "0",
        hours: 1,
        breaks: 2
    }
```

## [GET] Settings
Get the settings of the dashboard and see when the person took a break and when his work/study day was over. [www.dylanvens.com/iot-week-3/data/settings.json](http://www.dylanvens.com/iot-week-3/data/settings.json)

```json
    {
        pause: {
        time: "90 seconds",
        startTime: "2016-05-19 12:07:05",
        endTime: "2016-05-19 12:08:35"
        },
        work: {
        time: "120 seconds",
        startTime: "2016-05-19 12:07:05",
        endTime: "2016-05-19 12:09:05"
        }
    }
```

## [GET] Real time data
The dashboard gives back real time data. The user can access the data by entering the following url: [http://www.dylanvens.com/iot-week-3/includes/app/getData.php?limit=30](http://www.dylanvens.com/iot-week-3/includes/app/getData.php?limit=30). The user can set the limit with the amount of items that will be returned (?limit=40). Value equal to 0 is someone is not sitting. Value equal to 1 is someone is sitting. The Api will return an array of: 

```json
    {
        ID: "226",
        value: "0",
        date: "2016-05-19 12:09:20"
    }
```

## [POST] Settings
If you want to update or reset the settings you can do it by the following url: [www.dylanvens.com/iot-week-3/includes/app/setSettings.php](http://www.dylanvens.com/iot-week-3/includes/app/setSettings.php). It accepts the following strings: 

```
    pause=30seconds&work=30seconds
```

## [POST] Set status
You can change the status of the buzzer and leds with the following url: url: [www.dylanvens.com/iot-week-3/includes/app/setStatus.php](http://www.dylanvens.com/iot-week-3/includes/app/setStatus.php). It accepts the following strings:
```
    yellowLed=off&redLed=on&greenLed=off&buzzer=on
``` 