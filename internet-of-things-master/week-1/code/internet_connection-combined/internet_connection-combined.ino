// Include libraries
#include <ESP8266WiFi.h>
#include <ThingSpeak.h>
#include <EIoTCloudRestApi.h>
#include <EIoTCloudRestApiConfig.h>

// Constant Variables or char ssid[], char password[]
const char* ssid = "Dylan";
const char* pass = "123dvens";

// Pot 5706796dc943a0661cf314d7/LhDoJD7BykorHDRs
// Sound 5706796dc943a0661cf314d7/u9aopqArNwPuHKax
const char* EIOT_CLOUD_INSTANCE_PARAM_ID = "5706796dc943a0661cf314d7/LhDoJD7BykorHDRs";

// Variables
int sensorPotentionPin = A0; 
int sensorPotentionValue = 0; 
int ledPin = BUILTIN_LED;

// Init wificlient library
WiFiClient client;

// ThingSpeak settings
unsigned long channelNumber = 106644;
const char * channelWriteApiKey = "SAO5WUGNYFX8LQRW";

// Create CloudRestApi
EIoTCloudRestApi eiotcloud;

void setup() {
  Serial.begin(9600);
  pinMode (ledPin, OUTPUT);
  WiFi.begin(ssid, pass);
  ThingSpeak.begin(client);
  eiotcloud.begin();
  
  Serial.print("Connecting to ");
  Serial.println(ssid);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

}

void loop(){
  sensorPotentionValue = analogRead (sensorPotentionPin);
  
  // HIGH IS OFF
  // LOW IS ON
  digitalWrite (ledPin, HIGH);
  Serial.println (sensorPotentionValue, DEC);

  // Pot 2
  // Sound 1
  ThingSpeak.writeField(channelNumber, 2, sensorPotentionValue, channelWriteApiKey);
  eiotcloud.sendParameter(EIOT_CLOUD_INSTANCE_PARAM_ID, sensorPotentionValue);

  if (sensorPotentionValue > 60) {
    Serial.println("Klap Klap");
    digitalWrite (ledPin, LOW);
  }
  
}

