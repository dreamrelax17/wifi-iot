// Made by Dylan Vens - 2016

// Include libs
#include <ESP8266WiFi.h>

// Init wifi 
WiFiClient client;

// Wifi config, ssid = wifi name & password = wifi password
const char* ssid = "Dylan";  
const char* password = "123dvens";

// Get data host & port
const char* host = "www.dylanvens.com"; // Your domain  
const int httpPort = 80;

// post string & get json url
String postStr = "";
String path = "/iot/data/light.json";

// Delay values
const long postDelay = 5000;

// Sensor values
int sensorPotentionValue = 0; 

void setup() {  
  
  Serial.begin(9600);

  // Connect to wifi
  connectWifi();

  // Print wifi status
  printWifiStatus();
   
}

void loop() {  

  //postData();
  //getData();
  
}

void connectWifi() {

  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
}

void printWifiStatus() {
  // Print the SSID of the network you're attached to
  Serial.print("Connected to ");
  Serial.println(ssid);

  // Print your WiFi shield's IP address
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  
}

void setDelay() {
  delay(postDelay);
}

void postData() {
  
  // Get sensorValue
  sensorPotentionValue = analogRead(sensorPotentionPin);

  // Bind sensor value to post url
  postStr += "value=" + String(sensorPotentionValue);

  // If there's a successful connection, send the HTTP POST request
  if (client.connect(host, 80)) {

    client.println("POST /iot/includes/app/addItem.php HTTP/1.1");
    client.println("Host: www.dylanvens.com");
    client.println("Connection: close");
    client.println("Content-Type: application/x-www-form-urlencoded;");
    client.print("Content-Length: ");
    client.println(postStr.length());
    client.println();
    client.println(postStr);

    // Print post data
    while(client.available()) {
      char c = client.read();
      Serial.print(c);    
    }
    
    // give post enough time 
    delay(500); 

    // Print post url + data
    Serial.println(postStr);

    // Empty string
    postStr = "";
    
    
  } else {
    
    // If connection failed
    Serial.println("UH OH! Connection failed");
    
  }
 
}

void getData() {
  
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }

  // Used http://blog.nyl.io/esp8266-led-arduino/ for the code underneed
  client.print(String("GET ") + path + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" + 
               "Connection: keep-alive\r\n\r\n");

  // read response
  String section="header";
  while(client.available()){

    String line = client.readStringUntil('\r');
    
    // Serial.print(line);
    // we’ll parse the HTML body here
    if (section=="header") { // headers..
      
      Serial.print(".");
      
      if (line=="\n") { // skips the empty space at the beginning 
        section="json";
      }
      
    } else if (section=="json") {  // print the good stuff
      section="ignore";
      String result = line.substring(1);

      // Parse JSON
      int size = result.length() + 1;
      char json[size];
      result.toCharArray(json, size);
      StaticJsonBuffer<200> jsonBuffer;
      JsonObject& json_parsed = jsonBuffer.parseObject(json);
      
      if (!json_parsed.success()) {
        Serial.println("parseObject() failed");
        return;
      }

      // Make the decision to turn off or on the LED
      if (strcmp(json_parsed["light"], "on") == 0) {
        digitalWrite(pin, HIGH); 
        Serial.println("LED ON");
      } else {
        digitalWrite(pin, LOW);
        Serial.println("led off");
      }
      
    }
    
  }
  
}

