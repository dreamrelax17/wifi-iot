// Include libraries
#include <ThingSpeak.h>
#include <ESP8266WiFi.h>

// Constant Variables or char ssid[], char password[]
const char* ssid = "...";
const char* pass = "...";

// Variables
int sensorPotentionPin = A0; 
int sensorPotentionValue = 0; 
int ledPin = BUILTIN_LED;

// Init wificlient library
WiFiClient client;

// ThingSpeak settings
unsigned long channelNumber = 106644;
const char * channelWriteApiKey = "SAO5WUGNYFX8LQRW";

void setup() {
  pinMode (ledPin, OUTPUT);
  Serial.begin(9600);
  WiFi.begin(ssid, pass);
  ThingSpeak.begin(client);

  Serial.print("Connecting to ");
  Serial.println(ssid);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
}

void loop(){
  sensorPotentionValue = analogRead (sensorPotentionPin);
  
  // HIGH IS OFF
  // LOW IS ON
  digitalWrite (ledPin, HIGH);
  Serial.println (sensorPotentionValue, DEC);
  // 1 is channel 1, 2 is channel 2
  ThingSpeak.writeField(channelNumber, 1, sensorPotentionValue, channelWriteApiKey);

  if (sensorPotentionValue > 60) {
    Serial.println("Klap Klap");
    digitalWrite (ledPin, LOW);
  }
  
}

