#include <ESP8266WiFi.h>
#include <ArduinoJson.h>

const char* ssid     = "Bleh_Quest";  
const char* password = "123dvens";

const char* host     = "www.dylanvens.com"; // Your domain  
String path          = "/iot/data/light.json";  
const int pin        = 5;

int sensorPotentionPin = A0; 
int sensorPotentionValue = 0; 

void setup() {  
  pinMode(pin, OUTPUT); 
  pinMode(pin, HIGH);
  Serial.begin(9600);

  delay(10);
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);
  int wifi_ctr = 0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("WiFi connected");  
  Serial.println("IP address: " + WiFi.localIP());
}

void loop() {  
  Serial.print("connecting to ");
  Serial.println(host);

  getData();
  postData();
  
}

void postData(){

  WiFiClient client;

  sensorPotentionValue = analogRead (sensorPotentionPin);
  //Serial.println(sensorPotentionValue);
  
  String postStr = "value=" + String(sensorPotentionValue);
  while(client.available()) {
    
    client.print("POST /addItem.php HTTP/1.1\n");
    client.print("Host: www.dylanvens.com/iot/includes/app\n");
    client.print("Connection: close\n");
    client.print("Content-Type: application/x-www-form-urlencoded\n");
    client.print("Content-Length: ");
    client.print(postStr.length());
    client.print("\n\n");
    client.println(postStr);
    delay(700); // give post enough time
  
    char c = client.read();
    Serial.print(c);  
  }
  
  Serial.println(postStr);
  
}

void getData() {

  WiFiClient client;
  
  const int httpPort = 80;
  
  if (!client.connect(host, httpPort)) {
    Serial.println("connection failed");
    return;
  }

  client.print(String("GET ") + path + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" + 
               "Connection: keep-alive\r\n\r\n");

  delay(500); // wait for server to respond

  // read response
  String section="header";
  while(client.available()){

    String line = client.readStringUntil('\r');
    // Serial.print(line);
    // we’ll parse the HTML body here
    if (section=="header") { // headers..
      Serial.print(".");
      if (line=="\n") { // skips the empty space at the beginning 
        section="json";
      }
    }
    else if (section=="json") {  // print the good stuff
      section="ignore";
      String result = line.substring(1);

      // Parse JSON
      int size = result.length() + 1;
      char json[size];
      result.toCharArray(json, size);
      StaticJsonBuffer<200> jsonBuffer;
      JsonObject& json_parsed = jsonBuffer.parseObject(json);
      if (!json_parsed.success())
      {
        Serial.println("parseObject() failed");
        return;
      }

      // Make the decision to turn off or on the LED
      if (strcmp(json_parsed["light"], "on") == 0) {
        digitalWrite(pin, HIGH); 
        Serial.println("LED ON");
      }
      else {
        digitalWrite(pin, LOW);
        Serial.println("led off");
      }
    }
    
  }
  
}

