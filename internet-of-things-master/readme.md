# Internet of things
## Week 1:
* I used two sensors potentiometer and a soundsensor.
* The potentiometer reacts on sliding and rotating and gives a value back between 0 and 1024.
* The soundsensor gives a output of high or low signal when a sound intensity is reached.
* Link to the code of week 1: [Link to the code](https://github.com/dvens/internet-of-things/tree/master/week-1/code)
* Link to IoT cloud: [Link to IoT cloud](https://thingspeak.com/channels/106644)

# Internet of things
## Week 2:
* I used PHP and MySQL Database to create a dashboard to show data of my arduino.
* The data is being pulled every 15 seconds out of the database and compares in to the old date if there is any difference the chart is being drawn again.
 * Link to the php dashboard: [PHP dashboard](https://www.dylanvens.com/iot/)
* Link to IoT code week 2: [Arduino code for POST and GET](https://github.com/dvens/internet-of-things/blob/master/week-1/code/making_things_talk-v2/making_things_talk-v2.ino)

* Link to IoT code week 3: [Research Raspberry Pi and NodeMCU](http://research.dylanvens.com)
