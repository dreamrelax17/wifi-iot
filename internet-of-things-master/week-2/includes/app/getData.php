<?php
	
	require('connection.php');

	function getData($conn) {

		$limit = isset($_GET['limit']) ? $_GET['limit'] : 10;

		$sql = "SELECT * FROM sensor ORDER BY date DESC LIMIT $limit";
	    $result = mysqli_query($conn, $sql) or die("Error in Selecting " . mysqli_error($conn));

	    // Create array
	    $sensorArray = Array();

	    while( $row = mysqli_fetch_assoc($result) ) {

	    	$sensorArray[] = $row;

	    }

	    header('Content-type: application/json');
		echo json_encode($sensorArray);

	}

	getData($conn);

?>