<?php 
	require("includes/app/functions.php");
	include("includes/_base/head.php");
?>

<?php include("includes/_base/header.php"); ?>

	<main>
		
		<?php include("includes/modules/lamp-buttons.php"); ?>
		<?php include("includes/modules/line-chart.php"); ?>

	</main> <!-- end main -->

<?php include("includes/_base/footer.php"); ?>