(function(){

	'use strict';

	var _switch = document.querySelector('.switch');
	var _loader = new Loader();
	var _timerDelay = 10000;
	var _chartId = '#chart';
	var _loadElement = document.querySelector('.loader');
	var _container = document.querySelector(_chartId);
	var _dataLimit = 8;
	var _firstTime = true;
	var _oldData;
	var _timer;
	var _dataTimer;

	if(_firstTime) {
		toggleLoader('show');
	}

	_switch.addEventListener('change', function(e) {

		var _isChecked = e.target.checked; 
		
		if(_isChecked) {

			_timer = setTimeout(function(){

				window.location = '?light=on';
				clearTimeout(_timer);

			}, 600);	
			

		} else {

			_timer = setTimeout(function(){

				window.location = '?light=off';
				clearTimeout(_timer);

			}, 600);

		}

	});

	function toggleLoader(elementShow) {

		if(elementShow == 'show') {

            _loadElement.classList.add('is-visible');
            _container.classList.add('is-hidden');
            return;

        }

        if(elementShow == 'hide') {

            _loadElement.classList.remove('is-visible');
            _container.classList.remove('is-hidden');
            return;

        }

	}

	function sortById (a, b) {
		return (a.ID - b.ID);
	}

	function getMaxOfArray(array) {
	  return Math.max.apply(null, array);
	}

	_dataTimer = setInterval(function(){

		_loader.load('GET', 'includes/app/getData.php?limit=' + _dataLimit).then(function(response){
			
			var _data = response;
			var _values = [];
			var _maxValue;

			_data.sort(sortById);

			_data.map(function(item){
				
				_values.push(item.value);

			});

			_maxValue = getMaxOfArray(_values);

			if ( JSON.stringify(_data) === JSON.stringify(_oldData) ) {

				console.log('No new data');

			} else {

				toggleLoader('show');

				_timer = setTimeout(function(){

					createChart(_data, _maxValue);

				}, 1000);
				
			}

		});

	}, _timerDelay);

	function createChart(data, maxValue) {

		console.log('New data', _container, ' is emptied');
		_container.innerHTML = '';
		_oldData = data;

		var _data = data;
    	var maxValue = maxValue;
    	var margin = { top: 20, right: 30, bottom: 160, left: 50};
    	var width = 600 - margin.left - margin.right;
    	var height = 400 - margin.top - margin.bottom;
    	var translate = { x : 102, y : 9 };

    	var _xScale;
      	var _yScale;
      	var _xAxis;
      	var _yAxis;
      	var _chart;

      	function createScale() {

	        // set xScale map from input domain to output range (used if x axis is text instead of numbers)
	        _xScale = d3.scale.ordinal().rangeRoundBands([0, width]);

	        // set yScale
	    	_yScale = d3.scale.linear().range([height, 0]);

	    }

	    function createAxis() {
	
	        // Set x and y axis begin bottom and left
	        _xAxis = d3.svg.axis()
	            .scale(_xScale)
	            .orient('bottom');  

	        _yAxis = d3.svg.axis()
	            .scale(_yScale)
	            .orient('left');

	    }

	    function createChart() {
	          // Create chart append width and height 
	          // Add extra group to position (fix-position)
	        _chart = d3.select(_chartId)  
	                .append('svg')
	                .attr('width', width + margin.left + margin.right)
	          .attr('height', height + margin.top + margin.bottom)
	          .append('g')
	          .attr('class', 'fix-position')
	          .attr('transform', 'translate(' + translate.x + ', ' + translate.y + ')');
	           
	    }

	    function initChart() {
	        
	          // Set domain
	        _yScale.domain([-1, maxValue]);
	        _xScale.domain(_data.map(function(d){ return d.date }));

	        // Create tooltip and place a div in the body with visibility not shown
	        var tooltip = d3.select('body')
	          .append('div')
	          .attr('id', 'tooltip')
	          .style('position', 'absolute')
	          .style('z-index', '10')
	          .style('visibility', 'hidden');

	        // Create line-1 set interpolate to linear (for none curved lines or cardinal to make curved lines)
	        var line = d3.svg.line()
	              .interpolate('linear')  
	              .x(function(d) { return _xScale(d.date); })
	              .y(function(d) { return _yScale(d.value); });
	          
			// Draw line and attr data-item and class for a filter function
			// Call line(_data) function to drawn line with the data
			_chart.append('g')
				.attr('class', 'line-1')
				.attr('data-item', 'line-1')  
				.append('path')
				.attr('d', line(_data))
				.attr('class', 'line')
				.attr('stroke', '#6cc489')
				.attr('stroke-width', 2)
				.attr('fill', 'none');  
	        
	          // Position lines and attr data-item for filter 
	          // Return position for cx and cy (x and y coordinates of the center of the line)
	        _chart.selectAll('line-1')
	            .data(_data)
	            .enter()
	            .append('circle')
	            .attr('data-item', 'line-1')
	            .attr('data-number', _data.value)
	            .attr({
	                cx: function (d) { return _xScale(d.date);},
	                cy: function (d) { return _yScale(d.value); },
	                r: 4,
	                class: 'line-1-cirkel'
	            })
	            // Create mouseover for tooltip (event.pageX or Y gives position of mouse coordinates)
	            .on('mouseover', function(d) { 

	              d3.select('#tooltip')
	                .style('visibility', 'visible')
	                .style('left', (d3.event.pageX + 25) + 'px')
	                    .style('top', (d3.event.pageY - 30) + 'px')
	                    .text('Value: ' + d.value);

	            })
				.on('mouseout', function() { 
					return tooltip.style('visibility', 'hidden');
				});

	        // Create and set line chart
	        // Set anchor to begin at the starting point of the line
			_chart.append('g')
				.attr('transform', 'translate(' + (margin.left - translate.x + 22 ) + ',' + (height) + ')')
				.attr('class', 'x axis')   
				.call(_xAxis)
				.selectAll('text')  
				.style('text-anchor', 'end')
				.attr('dx', '-15px')
				.attr('transform', 'rotate(-65)');
 
			_chart.append('g')
				.attr('transform', 'translate(' + (margin.left - translate.x + 22 ) + ', 0)')
				.attr('class', 'y axis')
				.call(_yAxis);

			_chart.selectAll('circle')
				.data(_data)
				.enter()
				.append('div');
			
			// Create axis text 
			_chart.append('text')
				.attr('text-anchor', 'end')
				.attr('y', ( 6- translate.x ))
				.attr('dy', '0px')
				.attr('transform', 'rotate(-90)')
				.text('Value of the sensor');

			toggleLoader('hide');

	    }

	    function init() {
        	createScale();
        	createAxis();
        	createChart();
        	initChart();
        	_firstTime = false;
      	}

		init();	

	}

	// Button events
	var _buttons = [].slice.call(document.querySelectorAll('.button--click'));
	var _buttonLoader = new Loader();

	_buttons.forEach(function(button){

		console.log(button);

		button.addEventListener('click', function(e){

			var _url = this.getAttribute('href');

			console.log(_url);

			_buttonLoader.load('GET', _url).then(function(response){

				console.log(response);

			});

			e.preventDefault();

		});

	});

}());