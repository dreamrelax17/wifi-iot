(function(){

	'use strict';

	var _loader = new Loader();
	var _timerDelay = 10000;
	var _loadElement = document.querySelector('.loader');
	var _container = document.querySelector('main');
	var _dashboard = document.querySelector('.dashboard__weather');
	var _temp = document.querySelector('.dashboard__temprature');
	var _hum = document.querySelector('.container-humit');
	var _dashboardDay = document.querySelector('.dashboard__clock-day');
	var _dashboardMonth = document.querySelector('.dashboard__clock-month');
	var _month = moment().format('MMMM D, YYYY');
	var _dataLimit = 8;
	var _firstTime = true;
	var _oldData;
	var _timer;
	var _dataTimer;

	_dashboardMonth.innerHTML = _month;

	if(_dashboard) {
		renderHome();
	}

	function toggleLoader(elementShow) {

		if(elementShow == 'show') {

            _loadElement.classList.add('is-visible');
            _container.classList.add('is-hidden');
            return;

        }

        if(elementShow == 'hide') {

            _loadElement.classList.remove('is-visible');
            _container.classList.remove('is-hidden');
            return;

        }

	}

	function renderHome() {

		_dataTimer = setInterval(function(){

			_loader.load('GET', 'includes/app/getData.php?limit=' + _dataLimit).then(function(response){

				var _data = response;

				if ( JSON.stringify(_data) === JSON.stringify(_oldData) ) {

					console.log('No new data');

				} else {

					toggleLoader('show');

					_timer = setTimeout(function(){

						renderWeather(_data);

					}, 1000);

				}

			});

		}, _timerDelay);


		function renderWeather(data) {

			var _data = data.shift();
			var _dataTemp = _data.temprature;
			var _dataHum = _data.humidity;

			var _content = {
				temp: _dataTemp,
				hum: _dataHum
			}

			if(_dataTemp < 13) {

				_dashboard.classList.remove('dashboard__weather--summer');
				_dashboard.classList.add('dashboard__weather--cold');
				_temp.innerHTML = Peach.render('cold', _content, 'render');

			} else {

				_dashboard.classList.add('dashboard__weather--summer');
				_dashboard.classList.remove('dashboard__weather--cold');
				_temp.innerHTML = Peach.render('warm', _content, 'render');

			}

			_hum.innerHTML = Peach.render('hum', _content, 'render');

			_oldData = data;
			toggleLoader('hide');

		}

	}

	function displayTime() {
		var _day = moment().format('dddd HH:mm a');
		_dashboardDay.innerHTML = _day;
	}

	setInterval(displayTime, 1000);

}());
