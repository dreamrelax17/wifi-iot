<?php

	$light = isset($_GET['light']) ? $_GET['light'] : '';

	if( ISSET($_GET['light']) ) {

		if($light == "on") {
			$file = fopen("./data/light.json", "w") or die("can't open file");
			fwrite($file, '{"light": "on"}');
			fclose($file);
		} else if ($light == "off") {
			$file = fopen("./data/light.json", "w") or die("can't open file");
			fwrite($file, '{"light": "off"}');
			fclose($file);
		}

	}

?>

<section class="buttons -tx-center">

	<h2>
		<?php
			if($light=="on") {
				echo("Turn LED on.");
			}
			else if ($light=="off") {
				echo("Turn LED off.");
			}
			else {
				echo ("Turn the LED off or on");
			}
		?>
	</h2>

	<div class="switch switch--one">
  		<input id="switch-1" class="switch__toggle" <?php if($light == "on") { echo "checked"; } ?> type="checkbox">
  		<label for="switch-1"></label>
	</div>

</section>
