<section class="dashboard -tx-center">

		<div class="dashboard__weather">

			<div class="dashboard__temprature">

			</div>

			<div class="dashboard__clock">
				<img src="img/icon-time.svg" alt="" />
				<div class="container-clock">
					<div class="dashboard__clock-day"></div>
					<div class="dashboard__clock-month"></div>
				</div>
			</div>

			<div class="dashboard__humidity">
				<img src="img/humidity.svg" alt="" />
				<div class="container-humit">

				</div>
			</div>

		</div>

</section>

<template id="cold">
	<p>
		{{temp}}<span>&#8451</span><!---;&#8457;-->
	</p>
	<img src="img/cloud.svg" alt="" />
</template>

<template id="warm">
	<p>
		{{temp}} <span>&#8451</span><!---;&#8457;-->
	</p>
	<img src="img/Sun.svg" alt="" />
</template>

<template id="hum">
	<p>
		{{hum}}%
	</p>
</template>
