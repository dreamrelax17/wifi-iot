
// Include the libraries
#include <dht.h>

// Define pin sensor
int dht_apin = A0

// Bind the DHT library
dht DHT;
 
void setup(){
 
  Serial.begin(9600);
 
}
 
void loop(){
 
  readSensor();
  
  delay(5000);
 
}

// Declared dht sensor function
void readSensor() {
  DHT.read11(dht_apin);
 
  Serial.print("Current humidity = ");
  
  Serial.print(DHT.humidity);
  
  Serial.print("%  ");
  
  Serial.print("temperature = ");
  
  Serial.print(DHT.temperature); 
  Serial.println("C  ");
  
}

