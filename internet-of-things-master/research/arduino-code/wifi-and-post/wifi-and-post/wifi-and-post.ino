// Made by Dylan Vens - 2016

// Include libs
#include <ESP8266WiFi.h>

// Init wifi 
WiFiClient client;

// Wifi config, ssid = wifi name & password = wifi password
const char* ssid = "Dylan";  
const char* password = "123dvens";

// Get data host & port
const char* host = "www.dylanvens.com"; // Your domain  

void setup() {  
  
  Serial.begin(9600);

  // Connect to wifi
  connectWifi();

  // Print wifi status
  printWifiStatus();
   
}

void loop() {  

  postData();
  
}

void connectWifi() {

  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
}

void printWifiStatus() {
  // Print the SSID of the network you're attached to
  Serial.print("Connected to ");
  Serial.println(ssid);

  // Print your WiFi shield's IP address
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  
}

void postData() {
  
  // Bind sensor value to post url;
  postStr += "value=" + 20; // change the 20 to your own sensor value

  // If there's a successful connection, send the HTTP POST request
  if (client.connect(host, 80)) {

    client.println("POST /iot/includes/app/addItem.php HTTP/1.1");
    client.println("Host: www.dylanvens.com");
    client.println("Connection: close");
    client.println("Content-Type: application/x-www-form-urlencoded;");
    client.print("Content-Length: ");
    client.println(postStr.length());
    client.println();
    client.println(postStr);

    // Print post data
    while(client.available()) {
      char c = client.read();
      Serial.print(c);    
    }
    
    // give post enough time 
    delay(500); 

    // Print post url + data
    Serial.println(postStr);

    // Empty string again for the next data or it will add up :)
    postStr = "";
    
    
  } else {
    
    // If connection failed
    Serial.println("UH OH! Connection failed");
    
  }
 
}
